<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity()
 */
class Contact
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid", unique=true)
     * @ApiProperty(identifier=true)
     */
    private string $id;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private string $name;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private ?string $title;


    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private ?string $firstName;


    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private ?string $lastName;

    /**
     * @ORM\ManyToOne(targetEntity="Contact", inversedBy="contacts")
     * @ORM\JoinColumn()
     */
    private ?Contact $parentContact;

    /**
     * @var Collection<Address>
     * @ORM\OneToMany(targetEntity="Address", mappedBy="contact")
     */
    private Collection $addresses;

    /**
     * @var Collection<Phone>
     * @ORM\OneToMany(targetEntity="Phone", mappedBy="contact")
     */
    private Collection $phones;

    /**
     * @var Collection<Email>
     * @ORM\OneToMany(targetEntity="Email", mappedBy="contact")
     */
    private Collection $emails;

    /**
     * @var Collection<Contact>
     * @ORM\OneToMany(targetEntity="Contact", mappedBy="parentContact")
     */
    private Collection $contacts;

    public function __construct()
    {
        $this->addresses = new ArrayCollection();
        $this->phones = new ArrayCollection();
        $this->emails = new ArrayCollection();
        $this->contacts = new ArrayCollection();
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function setId(string $id): Contact
    {
        $this->id = $id;
        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): Contact
    {
        $this->name = $name;
        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): Contact
    {
        $this->title = $title;
        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(?string $firstName): Contact
    {
        $this->firstName = $firstName;
        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(?string $lastName): Contact
    {
        $this->lastName = $lastName;
        return $this;
    }

    public function getParentContact(): ?Contact
    {
        return $this->parentContact;
    }

    public function setParentContact(?Contact $parentContact): Contact
    {
        $this->parentContact = $parentContact;
        return $this;
    }

    /**
     * @return Collection<Address>
     */
    public function getAddresses(): Collection
    {
        return $this->addresses;
    }

    /**
     * @param Collection<Address> $addresses
     */
    public function setAddresses(Collection $addresses): Contact
    {
        $this->addresses = $addresses;
        return $this;
    }

    /**
     * @return Collection<Phone>
     */
    public function getPhones(): Collection
    {
        return $this->phones;
    }

    /**
     * @param Collection<Phone> $phones
     */
    public function setPhones(Collection $phones): Contact
    {
        $this->phones = $phones;
        return $this;
    }

    /**
     * @return Collection<Email>
     */
    public function getEmails(): Collection
    {
        return $this->emails;
    }

    /**
     * @param Collection<Email> $emails
     */
    public function setEmails(Collection $emails): Contact
    {
        $this->emails = $emails;
        return $this;
    }

    /**
     * @return Collection<Contact>
     */
    public function getContacts(): Collection
    {
        return $this->contacts;
    }

    /**
     * @param Collection<Contact> $contacts
     */
    public function setContacts(Collection $contacts): Contact
    {
        $this->contacts = $contacts;
        return $this;
    }
}