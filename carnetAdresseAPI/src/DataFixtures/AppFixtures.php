<?php

namespace App\DataFixtures;

use App\Entity\Address;
use App\Entity\Contact;
use App\Entity\Email;
use App\Entity\Phone;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Faker\Generator;
use Faker\Provider\Person;

class AppFixtures extends Fixture
{
    private Generator $faker;

    public function __construct()
    {
        $this->faker = Factory::create('fr_FR');
        $this->faker->seed(1);
    }

    public function load(ObjectManager $manager): void
    {
        for ($i = 0; $i < 100; $i++) {
            $this->newContact($manager);
        }

        for ($i = 0; $i < 10; $i++) {
            $this->newGroupe($manager);
        }

        $manager->flush();
    }

    private function newGroupe(ObjectManager $manager): void
    {
        $groupe = (new Contact())
            ->setId($this->faker->uuid())
            ->setName($this->faker->company());

        //Address
        if ($this->faker->randomDigit() >= 9) {
            $this->addAddress($manager, $groupe, Address::TYPE_SIEGE);
        }

        //Telephone
        if ($this->faker->randomDigit() >= 9) {
            $this->addPhone($manager, $groupe, Phone::TYPE_PRINCIPALE);
        }

        //Email
        if ($this->faker->randomDigit() >= 9) {
            $this->addEmail($manager, $groupe, Email::TYPE_PRINCIPALE);
        }

        //Contact
        $nbContact = $this->faker->randomDigitNotNull();
        for ($i = 0; $i < $nbContact; $i++) {
            $contact = $this->newContact($manager);
            $contact->setParentContact($groupe);
        }

        $manager->persist($groupe);
    }

    private function newContact(ObjectManager $manager): Contact
    {
        $gender = $this->faker->randomElement([Person::GENDER_MALE, Person::GENDER_FEMALE]);
        $firstName = $this->faker->firstName($gender);
        $lastName = $this->faker->lastName();
        $contact = (new Contact())
            ->setId($this->faker->unique->uuid())
            ->setFirstName($firstName)
            ->setLastName($lastName)
            ->setTitle($this->faker->title($gender))
            ->setId($this->faker->unique->uuid())
            ->setName($firstName . ' ' . $lastName);

        //Address
        if ($this->faker->randomDigit() > 1) {
            $this->addAddress($manager, $contact, Address::TYPE_PRINCIPALE);
            if ($this->faker->randomDigit() >= 9) {
                $this->addAddress($manager, $contact, Address::TYPE_SECONDAIRE);
            }
        }
        //Telephone
        if ($this->faker->randomDigit() > 1) {
            $this->addPhone($manager, $contact, Phone::TYPE_PRINCIPALE);
            if ($this->faker->randomDigit() >= 9) {
                $this->addPhone($manager, $contact, Phone::TYPE_SECONDAIRE);
            }
        }
        //Email
        if ($this->faker->randomDigit() > 1) {
            $this->addEmail($manager, $contact, Email::TYPE_PRINCIPALE);
            if ($this->faker->randomDigit() >= 9) {
                $this->addEmail($manager, $contact, Email::TYPE_SECONDAIRE);
            }
        }

        $manager->persist($contact);

        return $contact;
    }

    private function addAddress(ObjectManager $manager, Contact $contact, string $type): void
    {
        $adress = (new Address())
            ->setId($this->faker->unique->uuid())
            ->setCity($this->faker->city())
            ->setCountry('France')
            ->setPostcode($this->faker->postcode())
            ->setStreetAddress($this->faker->streetAddress())
            ->setType($type)
            ->setContact($contact);

        $manager->persist($adress);
    }

    private function addPhone(ObjectManager $manager, Contact $contact, string $type): void
    {
        $phone = (new Phone())
            ->setId($this->faker->unique->uuid())
            ->setType($type)
            ->setPhoneNumber($this->faker->phoneNumber())
            ->setContact($contact);

        $manager->persist($phone);
    }

    private function addEmail(ObjectManager $manager, Contact $contact, string $type): void
    {
        $email = (new Email())
            ->setId($this->faker->unique->uuid())
            ->setType($type)
            ->setEmail($this->faker->email())
            ->setContact($contact);

        $manager->persist($email);
    }
}
