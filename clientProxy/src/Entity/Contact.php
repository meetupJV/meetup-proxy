<?php

namespace App\Entity;


class Contact
{
    private string $id;

    private string $name;

    private ?string $title;

    private ?string $firstName;

    private ?string $lastName;

    private ?Contact $parentContact;

    /**
     * @var array<Address>
     */
    private array $addresses;

    /**
     * @var array<Address>
     */
    private array $phones;

    /**
     * @var array<Address>
     */
    private array $emails;

    /**
     * @var array<Address>
     */
    private array $contacts;

    public function __construct()
    {
        $this->addresses = [];
        $this->phones = [];
        $this->emails = [];
        $this->contacts = [];
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function setId(string $id): Contact
    {
        $this->id = $id;
        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): Contact
    {
        $this->name = $name;
        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): Contact
    {
        $this->title = $title;
        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(?string $firstName): Contact
    {
        $this->firstName = $firstName;
        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(?string $lastName): Contact
    {
        $this->lastName = $lastName;
        return $this;
    }

    public function getParentContact(): ?Contact
    {
        return $this->parentContact;
    }

    public function setParentContact(?Contact $parentContact): Contact
    {
        $this->parentContact = $parentContact;
        return $this;
    }

    /**
     * @return array<Address>
     */
    public function getAddresses(): array
    {
        return $this->addresses;
    }

    /**
     * @param array<Address> $addresses
     */
    public function setAddresses(array $addresses): Contact
    {
        $this->addresses = $addresses;
        return $this;
    }

    /**
     * @return array<Phone>
     */
    public function getPhones(): array
    {
        return $this->phones;
    }

    /**
     * @param array<Phone> $phones
     */
    public function setPhones(array $phones): Contact
    {
        $this->phones = $phones;
        return $this;
    }

    /**
     * @return array<Email>
     */
    public function getEmails(): array
    {
        return $this->emails;
    }

    /**
     * @param array<Email> $emails
     */
    public function setEmails(array $emails): Contact
    {
        $this->emails = $emails;
        return $this;
    }

    /**
     * @return array<Contact>
     */
    public function getContacts(): array
    {
        return $this->contacts;
    }

    /**
     * @param array<Contact> $contacts
     */
    public function setContacts(array $contacts): Contact
    {
        $this->contacts = $contacts;
        return $this;
    }
}