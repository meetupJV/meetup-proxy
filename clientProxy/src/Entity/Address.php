<?php

namespace App\Entity;


class Address
{
    public const TYPE_PRINCIPALE = 'principale';
    public const TYPE_SECONDAIRE = 'secondaire';
    public const TYPE_SIEGE = 'siége';

    private string $id;

    private string $type;

    private string $streetAddress;

    private string $postcode;

    private string $city;

    private string $country;

    private Contact $contact;

    public function getId(): string
    {
        return $this->id;
    }

    public function setId(string $id): Address
    {
        $this->id = $id;
        return $this;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function setType(string $type): Address
    {
        $this->type = $type;
        return $this;
    }

    public function getStreetAddress(): string
    {
        return $this->streetAddress;
    }

    public function setStreetAddress(string $streetAddress): Address
    {
        $this->streetAddress = $streetAddress;
        return $this;
    }

    public function getPostcode(): string
    {
        return $this->postcode;
    }

    public function setPostcode(string $postcode): Address
    {
        $this->postcode = $postcode;
        return $this;
    }

    public function getCity(): string
    {
        return $this->city;
    }

    public function setCity(string $city): Address
    {
        $this->city = $city;
        return $this;
    }

    public function getCountry(): string
    {
        return $this->country;
    }

    public function setCountry(string $country): Address
    {
        $this->country = $country;
        return $this;
    }

    public function getContact(): Contact
    {
        return $this->contact;
    }

    public function setContact(Contact $contact): Address
    {
        $this->contact = $contact;
        return $this;
    }
}