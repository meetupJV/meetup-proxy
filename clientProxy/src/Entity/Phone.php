<?php

namespace App\Entity;


class Phone
{
    public const TYPE_PRINCIPALE = 'principale';
    public const TYPE_SECONDAIRE = 'secondaire';

    private string $id;

    private string $type;

    private string $phoneNumber;

    private Contact $contact;

    public function getId(): string
    {
        return $this->id;
    }

    public function setId(string $id): Phone
    {
        $this->id = $id;
        return $this;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function setType(string $type): Phone
    {
        $this->type = $type;
        return $this;
    }

    public function getPhoneNumber(): string
    {
        return $this->phoneNumber;
    }

    public function setPhoneNumber(string $phoneNumber): Phone
    {
        $this->phoneNumber = $phoneNumber;
        return $this;
    }

    public function getContact(): Contact
    {
        return $this->contact;
    }

    public function setContact(Contact $contact): Phone
    {
        $this->contact = $contact;
        return $this;
    }
}