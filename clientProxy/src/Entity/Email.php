<?php

namespace App\Entity;


class Email
{
    public const TYPE_PRINCIPALE = 'principale';
    public const TYPE_SECONDAIRE = 'secondaire';

    private string $id;

    private string $type;

    private string $email;

    private Contact $contact;

    public function getId(): string
    {
        return $this->id;
    }

    public function setId(string $id): Email
    {
        $this->id = $id;
        return $this;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function setType(string $type): Email
    {
        $this->type = $type;
        return $this;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): Email
    {
        $this->email = $email;
        return $this;
    }

    public function getContact(): Contact
    {
        return $this->contact;
    }

    public function setContact(Contact $contact): Email
    {
        $this->contact = $contact;
        return $this;
    }
}