<?php

namespace App\Manuel;

class BaseEntity
{
    private string $valeur;

    public function getValeur(): string
    {
        return $this->valeur;
    }

    public function setValeur(string $valeur): BaseEntity
    {
        $this->valeur = $valeur;
        return $this;
    }
}