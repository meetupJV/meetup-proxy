<?php

namespace App\Manuel;

class ProxyBaseEntity extends BaseEntity
{
    private bool $isInit = false;

    public function getValeur(): string
    {
        $this->init();
        return parent::getValeur();
    }

    private function init(): void
    {
        if ($this->isInit) {
            return;
        }

        $this->setValeur('test');
    }
}