<?php

namespace App\Repository;

use App\Client\CarnetAddressClient;

class AddressRepository extends AbstractRepository
{
    public CONST URL_FIND_ALL = '/api/addresses';
}