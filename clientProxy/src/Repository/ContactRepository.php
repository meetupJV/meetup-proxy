<?php

namespace App\Repository;

use App\Client\CarnetAddressClient;

class ContactRepository extends AbstractRepository
{
    public CONST URL_FIND_ALL = '/api/contacts';
}