<?php

namespace App\Repository;

use App\Client\CarnetAddressClient;

abstract class AbstractRepository
{
    public const URL_FIND_ALL = null;

    private CarnetAddressClient $client;

    public function __construct(CarnetAddressClient $client)
    {
        $this->client = $client;
    }

    public function findAll()
    {
        return $this->client->request('GET', static::URL_FIND_ALL);
    }

    public function findByRessourceId(string $ressourceId)
    {
        return $this->client->request('GET', $ressourceId);
    }
}