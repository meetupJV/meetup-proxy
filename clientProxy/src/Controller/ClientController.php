<?php

namespace App\Controller;


use App\Repository\ContactRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ClientController extends AbstractController
{
    /**
     * @Route(name="client", path="/client")
     */
    public function client(
        ContactRepository $contactRepository
    ): Response
    {
        $contact = $contactRepository->findAll();
        return $this->render('client.html.twig', [
            'contacts' => $contact,
        ]);
    }
}