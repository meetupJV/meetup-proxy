<?php

namespace App\Controller;

use App\Manuel\BaseEntity;
use App\Manuel\ProxyBaseEntity;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route(name="home", path="/")
     */
    public function home(): Response
    {
        return $this->render('home.html.twig');
    }
}