<?php

namespace App\Controller;

use App\Manuel\BaseEntity;
use ProxyManager\Factory\LazyLoadingValueHolderFactory;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProxyController extends AbstractController
{
    /**
     * @Route(name="proxy", path="/proxy")
     */
    public function proxy(): Response
    {
        $base = new BaseEntity();
//        $proxy = new ProxyBaseEntity();

        $factory = new LazyLoadingValueHolderFactory();
        $proxy = $factory->createProxy(
            BaseEntity::class,
            function (&$wrappedObject, $proxy, $method, $parameters, &$initializer) {
                dump('init');

                /** @var BaseEntity $wrappedObject */
                $wrappedObject = (new BaseEntity())
                    ->setValeur('bonjour');
                $initializer = null;

                return true;
            }
        );


        dump($proxy);
        return $this->render('proxy.html.twig', [
            'base' => $base,
            'proxy' => $proxy
        ]);
    }
}