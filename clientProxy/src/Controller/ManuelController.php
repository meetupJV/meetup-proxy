<?php

namespace App\Controller;

use App\Manuel\BaseEntity;
use App\Manuel\ProxyBaseEntity;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ManuelController extends AbstractController
{
    /**
     * @Route(name="manuel", path="/manuel")
     */
    public function manuel(): Response
    {
        return $this->render('manuel.html.twig', [
            'base' => new BaseEntity(),
            'proxy' => new ProxyBaseEntity()
        ]);
    }
}