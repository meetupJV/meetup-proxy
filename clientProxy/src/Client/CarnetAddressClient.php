<?php

namespace App\Client;

use JsonException;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class CarnetAddressClient
{
    private HttpClientInterface $client;
    private SerializerInterface $serializer;

    public function __construct(
        HttpClientInterface $carnet_address_client,
        SerializerInterface $serializer

    ) {
        $this->client = $carnet_address_client;
        $this->serializer = $serializer;
    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     * @throws JsonException
     */
    public function request(string $method, string $url, $updateObject = null)
    {
        $response = $this->client->request($method, $url);
        return $this->deserialize($response->getContent(), $updateObject);
    }

    /**
     * @throws JsonException
     */
    public function deserialize($data, $updateObject = null)
    {
        if (is_array($data)) {
            $data = json_encode($data, JSON_THROW_ON_ERROR);
        }

        $context = [
            'client' => $this
        ];

        if ($updateObject !== null) {
            $context[AbstractNormalizer::OBJECT_TO_POPULATE] = $updateObject;
        }

        return $this->serializer->deserialize($data, '', 'json', $context);
    }
}