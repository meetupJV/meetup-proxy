<?php

namespace App\Serializer\Normalizer;

use App\Client\CarnetAddressClient;
use ProxyManager\Factory\LazyLoadingGhostFactory;
use ProxyManager\Proxy\GhostObjectInterface;
use ReflectionException;
use ReflectionProperty;
use RuntimeException;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer as SFAbstractNormalizer;

class AbstractNormalizer
{
    /**
     * @return mixed
     * @throws ReflectionException
     */
    public function createProxy(CarnetAddressClient $client, string $className, string $resource)
    {
        $factory = new LazyLoadingGhostFactory();

        $initializer = static function (
            GhostObjectInterface $ghostObject,
            string $method,
            array $parameters,
            &$initializer
        ) use ($client) {
            if(method_exists($ghostObject, 'getId')) {
                $client->request('GET', $ghostObject->getId(), $ghostObject);
            } else {
                throw new RuntimeException('no getId() methode to find object');
            }
            $initializer = null;

            return true;
        };
        $proxyOptions = [
            'skippedProperties' => [
                "\x00" . $className . "\x00id",
            ],
        ];

        $instance = $factory->createProxy($className, $initializer, $proxyOptions);

        $idReflection = new ReflectionProperty($className, 'id');
        $idReflection->setAccessible(true);
        $idReflection->setValue($instance, $resource);

        return $instance;
    }

    public function getInstance(string $className, array $context = [])
    {
        return $context[SFAbstractNormalizer::OBJECT_TO_POPULATE] ?? new $className();
    }
}