<?php

namespace App\Serializer\Normalizer;

use App\Entity\Address;
use App\Entity\Contact;
use App\Entity\Email;
use ReflectionException;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;

class EmailNormalizer extends AbstractNormalizer implements DenormalizerInterface
{
    /**
     * @throws ReflectionException
     */
    public function denormalize($data, string $type, string $format = null, array $context = []): Email
    {
        $email = $this->getInstance(Email::class, $context);
        $email->setId($data['id']);
        $email->setType($data['type']);
        $email->setEmail($data['email']);
        $email->setContact($this->createProxy($context['client'], Contact::class, $data['contact']));

        return $email;
    }

    public function supportsDenormalization($data, string $type, string $format = null): bool
    {
        return $data['@type'] === 'Email';
    }
}