<?php

namespace App\Serializer\Normalizer;

use App\Client\CarnetAddressClient;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;

class CollectionNormalizer extends AbstractNormalizer implements DenormalizerInterface
{

    public function denormalize($data, string $type, string $format = null, array $context = []): array
    {
        /** @var CarnetAddressClient $client */
        $client = $context['client'];

        $values = [];
        foreach ($data['hydra:member'] as $ligne) {
            $values[] = $client->deserialize($ligne);
        }

        return $values;
    }

    public function supportsDenormalization($data, string $type, string $format = null)
    {
        return $data['@type'] === 'hydra:Collection';
    }
}