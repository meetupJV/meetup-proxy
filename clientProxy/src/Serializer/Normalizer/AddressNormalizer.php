<?php

namespace App\Serializer\Normalizer;

use App\Entity\Address;
use App\Entity\Contact;
use ReflectionException;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;

class AddressNormalizer extends AbstractNormalizer implements DenormalizerInterface
{
    /**
     * @throws ReflectionException
     */
    public function denormalize($data, string $type, string $format = null, array $context = []): Address
    {
        $address = $this->getInstance(Address::class, $context);
        $address->setId($data['id']);
        $address->setType($data['type']);
        $address->setStreetAddress($data['streetAddress']);
        $address->setPostcode($data['postcode']);
        $address->setCity($data['city']);
        $address->setCountry($data['country']);
        $address->setContact($this->createProxy($context['client'], Contact::class, $data['contact']));

        return $address;
    }

    public function supportsDenormalization($data, string $type, string $format = null): bool
    {
        return $data['@type'] === 'Address';
    }
}