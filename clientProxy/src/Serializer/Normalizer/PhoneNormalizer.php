<?php

namespace App\Serializer\Normalizer;

use App\Entity\Address;
use App\Entity\Contact;
use App\Entity\Email;
use App\Entity\Phone;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;

class PhoneNormalizer extends AbstractNormalizer implements DenormalizerInterface
{
    /**
     * @throws \ReflectionException
     */
    public function denormalize($data, string $type, string $format = null, array $context = []): Phone
    {
        $phone = $this->getInstance(Phone::class, $context);
        $phone->setId($data['id']);
        $phone->setType($data['type']);
        $phone->setPhoneNumber($data['phoneNumber']);
        $phone->setContact($this->createProxy($context['client'], Contact::class, $data['contact']));

        return $phone;
    }

    public function supportsDenormalization($data, string $type, string $format = null): bool
    {
        return $data['@type'] === 'Phone';
    }
}