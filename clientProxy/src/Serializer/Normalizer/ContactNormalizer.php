<?php

namespace App\Serializer\Normalizer;

use App\Entity\Address;
use App\Entity\Contact;
use App\Entity\Email;
use App\Entity\Phone;
use ReflectionException;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;

class ContactNormalizer extends AbstractNormalizer implements DenormalizerInterface
{
    /**
     * @throws ReflectionException
     */
    public function denormalize($data, string $type, string $format = null, array $context = []): Contact
    {
        $contact = $this->getInstance(Contact::class, $context);
        $contact->setId($data['id']);
        $contact->setName($data['name']);
        $contact->setTitle($data['title'] ?? null);
        $contact->setFirstName($data['firstName'] ?? null);
        $contact->setLastName($data['lastName'] ?? null);
        if (isset($data['parentContact'])) {
            $contact->setParentContact($this->createProxy($context['client'], Contact::class, $data['parentContact']));
        }

        $addresse = [];
        foreach ($data['addresses'] as $address){
            $addresse[] = $this->createProxy($context['client'], Address::class, $address);
        }
        $contact->setAddresses($addresse);

        $phones = [];
        foreach ($data['phones'] as $phone){
            $phones[] = $this->createProxy($context['client'], Phone::class, $phone);
        }
        $contact->setPhones($phones);

        $emails = [];
        foreach ($data['emails'] as $email){
            $emails[] = $this->createProxy($context['client'], Email::class, $email);
        }
        $contact->setEmails($emails);

        return $contact;
    }

    public function supportsDenormalization($data, string $type, string $format = null): bool
    {
        return $data['@type'] === 'Contact';
    }
}