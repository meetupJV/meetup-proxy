<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 */
class Contenu
{
    /**
     * @ORM\Id()
     * @ORM\Column()
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private int $id;

    /**
     * @ORM\Column()
     */
    private string $nom;

    /**
     * @ORM\ManyToOne(targetEntity="Chapeau", inversedBy="contenus")
     * @ORM\JoinColumn()
     */
    private Chapeau $chapeau;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): Contenu
    {
        $this->id = $id;
        return $this;
    }

    public function getNom(): string
    {
        return $this->nom;
    }

    public function setNom(string $nom): Contenu
    {
        $this->nom = $nom;
        return $this;
    }

    public function getChapeau(): Chapeau
    {
        return $this->chapeau;
    }

    public function setChapeau(Chapeau $chapeau): Contenu
    {
        $this->chapeau = $chapeau;
        return $this;
    }

}