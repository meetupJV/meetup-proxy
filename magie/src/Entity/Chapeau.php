<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 */
class Chapeau
{
    /**
     * @ORM\Id()
     * @ORM\Column()
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private int $id;

    /**
     * @ORM\Column()
     */
    private string $nom;

    /**
     * @ORM\OneToMany(targetEntity="Contenu", mappedBy="chapeau")
     * @var Collection<Contenu>
     */
    private Collection $contenu;

    public function __construct()
    {
        $this->contenu = new ArrayCollection();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): Chapeau
    {
        $this->id = $id;
        return $this;
    }

    public function getNom(): string
    {
        return $this->nom;
    }

    public function setNom(string $nom): Chapeau
    {
        $this->nom = $nom;
        return $this;
    }

    public function getContenu(): Collection
    {
        return $this->contenu;
    }

    public function setContenu(Collection $contenu): Chapeau
    {
        $this->contenu = $contenu;
        return $this;
    }

}