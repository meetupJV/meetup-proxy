<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 */
class Magicien
{
    /**
     * @ORM\Id()
     * @ORM\Column()
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private int $id;

    /**
     * @ORM\Column()
     */
    private string $nom;

    /**
     * @ORM\Column()
     */
    private string $assistante;

    /**
     * @ORM\OneToOne(targetEntity="Chapeau")
     */
    private Chapeau $chapeau;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): Magicien
    {
        $this->id = $id;
        return $this;
    }

    public function getNom(): string
    {
        return $this->nom;
    }

    public function setNom(string $nom): Magicien
    {
        $this->nom = $nom;
        return $this;
    }

    public function getAssistante(): string
    {
        return $this->assistante;
    }

    public function setAssistante(string $assistante): Magicien
    {
        $this->assistante = $assistante;
        return $this;
    }


    public function getChapeau(): Chapeau
    {
        return $this->chapeau;
    }

    public function setChapeau(Chapeau $chapeau): Magicien
    {
        $this->chapeau = $chapeau;
        return $this;
    }

}