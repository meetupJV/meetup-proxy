<?php

namespace App\Repository;

use App\Entity\Magicien;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Magicien|null find($id, $lockMode = null, $lockVersion = null)
 * @method Magicien|null findOneBy(array $criteria, array $orderBy = null)
 * @method Magicien[]    findAll()
 * @method Magicien[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MagicienRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Magicien::class);
    }
}