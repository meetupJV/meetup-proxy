<?php

namespace App\Controller;

use App\Repository\MagicienRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TourMagi extends AbstractController
{
    /**
     * @Route(name="home", path="/")
     */
    public function index(MagicienRepository $repository): Response
    {
        return $this->render('index.html.twig', ['magicien' => $repository->find(1)]);
    }

    /**
     * @Route(
     *     name="magie",
     *     path="/etp{etape}/{type}",
     *     defaults={"type":"exec"},
     *     requirements=
     *          {
     *              "etape"="1|2|3|4",
     *              "type"="code|exec",
     *          }
     *     )
     */
    public function magie(MagicienRepository $repository, string $etape, string $type): Response
    {
        $file = 'etp' . $etape . '.html.twig';

        if ($type === 'code') {
            return new BinaryFileResponse('../templates/' . $file);
        }

        return $this->render($file, ['magicien' => $repository->find(1)]);
    }
}